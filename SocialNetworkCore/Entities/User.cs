﻿using ValueObjects;

namespace Entities;

public class User
{
  public UserId Id { get; }
  public string PasswordHash { get; } 
  public string FirstName { get; }
  public string SecondName { get; }
  public DateOnly BirthDate { get; }
  public string Biography { get; }
  public string City { get; }

  public User(UserId id, string passwordHash, string firstName, string secondName, DateOnly birthDate, string biography, string city)
  {
    Id = id;
    PasswordHash = passwordHash;
    FirstName = firstName;
    SecondName = secondName;
    BirthDate = birthDate;
    Biography = biography;
    City = city;
  }
}