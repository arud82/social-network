﻿namespace Exceptions;

public class NotFoundException: Exception
{
  public NotFoundException(string message): base(message) { }

  public static NotFoundException Create<TEntity>(object key) => new($"{typeof(TEntity).Name} '{key}' not found");

  public static NotFoundException Create<TEntity>(object field, string fieldName) =>
    new($"{typeof(TEntity).Name} with '{fieldName}' = '{field}' not found");
}