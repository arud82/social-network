﻿namespace Exceptions;

public class BadInputDataException: Exception
{
  public BadInputDataException(string message): base(message) { }
}