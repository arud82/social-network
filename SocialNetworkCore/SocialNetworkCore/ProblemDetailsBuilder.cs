﻿using Exceptions;
using Hellang.Middleware.ProblemDetails;
using Microsoft.AspNetCore.Mvc;

namespace SocialNetworkCore;

public static class ProblemDetailsBuilder
{
  public static IServiceCollection AddAppProblemDetails(this IServiceCollection services)
  {
    services.AddProblemDetails(
      options =>
      {
        options.IncludeExceptionDetails = (_, _) => false;

        options.Map<BadInputDataException>((context, e) => CreateProblemDetails(context, StatusCodes.Status400BadRequest, e.Message));
        options.Map<ForbiddenException>((context, e) => CreateProblemDetails(context, StatusCodes.Status403Forbidden, e.Message));
        options.Map<NotFoundException>((context, e) => CreateProblemDetails(context, StatusCodes.Status404NotFound, e.Message));
      });

    return services;
  }

  private static ProblemDetails CreateProblemDetails(HttpContext context, int statusCode, string detail)
  {
    var problemDetailsFactory = context.RequestServices.GetRequiredService<ProblemDetailsFactory>();

    return problemDetailsFactory
      .CreateProblemDetails(
        context: context,
        statusCode: statusCode,
        detail: detail,
        instance: context.Request.Path);
  }
}