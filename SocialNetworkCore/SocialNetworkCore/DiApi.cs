﻿using SocialNetworkCore.Api.UserApi.GetUser;
using SocialNetworkCore.Api.UserApi.RegisterUser;

namespace SocialNetworkCore;

public static class DiApi
{
  public static void AddApi(this IServiceCollection services)
  {
    services.AddScoped<RegisterUserDtoBuilder>();

    services.AddScoped<UserOutputDataBuilder>();
  }
}