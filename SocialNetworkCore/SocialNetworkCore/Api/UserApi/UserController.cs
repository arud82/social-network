﻿using Exceptions;
using Microsoft.AspNetCore.Mvc;
using Services.Abstractions.UserServices;
using SocialNetworkCore.Api.UserApi.GetUser;
using SocialNetworkCore.Api.UserApi.RegisterUser;
using ValueObjects;

namespace SocialNetworkCore.Api.UserApi;

[ApiController]
public class UserController: ControllerBase
{
  private readonly IUserService _userService;
  private readonly RegisterUserDtoBuilder _registerUserDtoBuilder;
  private readonly UserOutputDataBuilder _userOutputDataBuilder;

  public UserController(
    IUserService userService,
    RegisterUserDtoBuilder registerUserDtoBuilder,
    UserOutputDataBuilder userOutputDataBuilder)
  {
    _userService = userService;
    _registerUserDtoBuilder = registerUserDtoBuilder;
    _userOutputDataBuilder = userOutputDataBuilder;
  }

  [HttpPost("/user/register")]
  public async Task<RegisterUserOutputData> RegisterUser(RegisterUserInputData inputData)
  {
    var userDto = _registerUserDtoBuilder.Build(inputData);
    var user = await _userService.Register(userDto);
    return new RegisterUserOutputData(user.Id.Value);
  }

  [HttpGet("/user/{id}")]
  public async Task<UserOutputData> GetUser(string id)
  {
    var userId = UserId.Create(id);

    if (userId == null)
      throw new BadInputDataException("invalid user_id");

    var user = await _userService.Get(userId);
    return _userOutputDataBuilder.Build(user);
  }

  // ReSharper disable InconsistentNaming
  [HttpGet("/user/search")]
  public async Task<IEnumerable<UserOutputData>> SearchUsers(string? first_name, string? last_name)
  {
    if (string.IsNullOrWhiteSpace(first_name) && string.IsNullOrWhiteSpace(last_name))
      throw new BadInputDataException("first_name or last_name must be set");

    var users = await _userService.Search(first_name, last_name);
    return users.Select(_userOutputDataBuilder.Build);
  }

  [HttpGet("/user/search/count")]
  public async Task<int> CountUsers(string? first_name, string? last_name)
  {
    if (string.IsNullOrWhiteSpace(first_name) && string.IsNullOrWhiteSpace(last_name))
      throw new BadInputDataException("first_name or last_name must be set");

    return await _userService.Count(first_name, last_name);
  }
  // ReSharper restore InconsistentNaming
}