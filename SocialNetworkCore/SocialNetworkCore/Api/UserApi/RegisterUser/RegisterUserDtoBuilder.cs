﻿using Services.Abstractions.UserServices;

namespace SocialNetworkCore.Api.UserApi.RegisterUser;

public class RegisterUserDtoBuilder
{
  public RegisterUserDto Build(RegisterUserInputData userInputData) =>
    new(
      FirstName: userInputData.FirstName,
      SecondName: userInputData.SecondName,
      BirthDate: userInputData.BirthDate,
      Biography: userInputData.Biography,
      City: userInputData.City,
      Password: userInputData.Password);
}