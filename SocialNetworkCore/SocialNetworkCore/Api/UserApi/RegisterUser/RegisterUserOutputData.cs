﻿using System.Text.Json.Serialization;

namespace SocialNetworkCore.Api.UserApi.RegisterUser;

public class RegisterUserOutputData
{
  [JsonPropertyName("user_id")]
  public Guid UserId { get; }

  public RegisterUserOutputData(Guid userId)
  {
    UserId = userId;
  }
}