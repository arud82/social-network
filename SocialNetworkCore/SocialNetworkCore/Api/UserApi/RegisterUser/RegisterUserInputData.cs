﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace SocialNetworkCore.Api.UserApi.RegisterUser;

public class RegisterUserInputData
{
  [Required]
  [JsonPropertyName("first_name")]
  public string FirstName { get; set; } = "";

  [Required]
  [JsonPropertyName("second_name")]
  public string SecondName { get; set; } = "";

  [Required]
  [JsonPropertyName("birthdate")]
  public DateOnly BirthDate { get; set; }

  [Required]
  [JsonPropertyName("biography")]
  public string Biography { get; set; } = "";

  [Required]
  [JsonPropertyName("city")]
  public string City { get; set; } = "";

  [Required]
  [JsonPropertyName("password")]
  public string Password { get; set; } = "";
}