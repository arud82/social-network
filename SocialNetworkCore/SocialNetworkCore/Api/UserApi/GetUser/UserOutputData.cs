﻿using System.Text.Json.Serialization;

namespace SocialNetworkCore.Api.UserApi.GetUser;

public class UserOutputData
{
  [JsonPropertyName("id")]
  public string Id { get; }

  [JsonPropertyName("first_name")]
  public string FirstName { get; }

  [JsonPropertyName("second_name")]
  public string SecondName { get; }

  [JsonPropertyName("birth_date")]
  public DateOnly BirthDate { get; }

  [JsonPropertyName("biography")]
  public string Biography { get; }

  [JsonPropertyName("city")]
  public string City { get; }

  public UserOutputData(
    string id,
    string firstName,
    string secondName,
    DateOnly birthDate,
    string biography,
    string city)
  {
    Id = id;
    FirstName = firstName;
    SecondName = secondName;
    BirthDate = birthDate;
    Biography = biography;
    City = city;
  }
}