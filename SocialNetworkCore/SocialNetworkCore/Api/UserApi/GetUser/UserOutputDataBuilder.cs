﻿using Entities;

namespace SocialNetworkCore.Api.UserApi.GetUser;

public class UserOutputDataBuilder
{
  public UserOutputData Build(User user) =>
    new(
      id: user.Id.ToString(),
      firstName: user.FirstName,
      secondName: user.SecondName,
      birthDate: user.BirthDate,
      biography: user.Biography,
      city: user.City);
}