﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace SocialNetworkCore.Api.Login;

public class LoginInputData
{
  [Required]
  [JsonPropertyName("id")]
  public string Id { get; set; } = "";

  [Required]
  [JsonPropertyName("password")]
  public string Password { get; set; } = "";
}