﻿using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using Exceptions;
using Microsoft.AspNetCore.Mvc;
using Services.Abstractions.UserServices;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using ValueObjects;
using Entities;

namespace SocialNetworkCore.Api.Login;

[ApiController]
public class LoginController: ControllerBase
{
  private readonly IAuthenticationService _authenticationService;
  private readonly JwtOptions _jwtOptions;

  public LoginController(IAuthenticationService authenticationService, JwtOptions jwtOptions)
  {
    _authenticationService = authenticationService;
    _jwtOptions = jwtOptions;
  }

  [HttpPost("/login")]
  public async Task<LoginOutputData> Login(LoginInputData inputData)
  {
    var userId = UserId.Create(inputData.Id);

    if (userId == null)
      throw new BadInputDataException("invalid user_id");

    var user = await _authenticationService.Authenticate(userId, inputData.Password);
    var jwtToken = GetJwtToken(user);

    return new LoginOutputData(jwtToken);
  }

  private string GetJwtToken(User user)
  {
    DateTime utcNow = DateTime.UtcNow;

    var claims = new[] {
      new Claim(JwtRegisteredClaimNames.Sub, _jwtOptions.Subject),
      new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
      new Claim(JwtRegisteredClaimNames.Iat, utcNow.ToString(CultureInfo.InvariantCulture)),
      new Claim("UserId", user.Id.ToString()),
    };

    var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtOptions.Key));

    var jwtToken = new JwtSecurityToken(
      _jwtOptions.Issuer,
      notBefore: utcNow,
      claims: claims,
      expires: utcNow.AddMinutes(_jwtOptions.LifetimeMin),
      signingCredentials: new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256));

    string encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwtToken);

    return encodedJwt;
  }
}