﻿using System.Text.Json.Serialization;

namespace SocialNetworkCore.Api.Login;

public class LoginOutputData
{
  [JsonPropertyName("token")]
  public string Token { get; }

  public LoginOutputData(string token)
  {
    Token = token;
  }
}