﻿namespace SocialNetworkCore.Api.Login;

public class JwtOptions
{
  public string Key { get; set; } = "";
  public string Issuer { get; set; } = "";
  public string Subject { get; set; } = "";
  public int LifetimeMin { get; set; }
}