using DbAccess;
using DbAccess.ConnectionFactory;
using DbMigrations;
using Hellang.Middleware.ProblemDetails;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Services;
using SocialNetworkCore.Api.Login;
using SocialNetworkCore.Middleware;
using System;
using System.Text;

namespace SocialNetworkCore;

public class Program
{
  public static void Main(string[] args)
  {
    var builder = WebApplication.CreateBuilder(args);

    AddConfiguration(builder);
    AddServices(builder);
    AddOptions(builder);

    var app = builder.Build();

    ConfigureRequestPipeline(app);
  }

  private static void AddConfiguration(WebApplicationBuilder builder)
  {
    string environmentName = builder.Environment.EnvironmentName;

    builder.Configuration.AddJsonFile(Path.Combine("DefaultConfigs", "appsettings.json"));
    builder.Configuration.AddJsonFile(Path.Combine("EnvironmentConfigs", $"appsettings.{environmentName}.json"), optional: true);
  }

  private static void AddServices(WebApplicationBuilder builder)
  {
    var serviceCollection = builder.Services;

    serviceCollection.AddServices();
    serviceCollection.AddDbAccess();
    serviceCollection.AddDbMigrationServices();
    serviceCollection.AddApi();

    serviceCollection.AddControllers();
    serviceCollection.AddEndpointsApiExplorer();
    serviceCollection.AddSwaggerGen();

    AddJwtAuthentication(serviceCollection, builder.Configuration);
    serviceCollection.AddAppProblemDetails();

    serviceCollection.AddHostedService<LifetimeEventsHostedService>();
  }

  private static void AddOptions(WebApplicationBuilder builder)
  {
    AddOptions<MigrationOptions>(builder, "ConnectionStrings");
    AddOptions<DbConnectionFactoryOptions>(builder, "ConnectionStrings");
    AddOptions<JwtOptions>(builder, "Jwt");
  }

  private static void AddOptions<T>(WebApplicationBuilder builder, string sectionName) where T: class
  {
    builder.Services.Configure<T>(builder.Configuration.GetSection(sectionName));
    builder.Services.AddSingleton<T>(sp => sp.GetRequiredService<IOptions<T>>().Value);
  }

  private static void AddJwtAuthentication(IServiceCollection services, IConfiguration configuration)
  {
    var issuer = configuration["Jwt:Issuer"];
    var key = configuration["Jwt:Key"];

    services
      .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
      .AddJwtBearer(
        bearerOptions =>
        {
          bearerOptions.TokenValidationParameters = new TokenValidationParameters
          {
            ValidateIssuer = true,
            ValidateAudience = false,
            ValidateLifetime = true,
            ValidateIssuerSigningKey = true,
            ValidIssuer = issuer,
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key!))
          };
        });
  }

  private static void ConfigureRequestPipeline(WebApplication app)
  {
    if (app.Environment.IsDevelopment() || app.Environment.IsStaging())
    {
      app.UseSwagger();
      app.UseSwaggerUI();
    }

    app.UseProblemDetails();
    app.UseMiddleware<CheckAppStateMiddleware>();

    app.UseAuthorization();
    app.MapControllers();

    app.Run();
  }
}