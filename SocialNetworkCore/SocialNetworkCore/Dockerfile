#See https://aka.ms/customizecontainer to learn how to customize your debug container and how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:7.0 AS base
WORKDIR /app
EXPOSE 80

FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build
WORKDIR /src
COPY ["SocialNetworkCore/SocialNetworkCore.csproj", "SocialNetworkCore/"]
COPY ["DbMigrations/DbMigrations.csproj", "DbMigrations/"]
COPY ["Entities/Entities.csproj", "Entities/"]
COPY ["ValueObjects/ValueObjects.csproj", "ValueObjects/"]
COPY ["Exceptions/Exceptions.csproj", "Exceptions/"]
COPY ["Repositories.Abstractions/Repositories.Abstractions.csproj", "Repositories.Abstractions/"]
COPY ["Repositories/DbAccess.csproj", "Repositories/"]
COPY ["Services.Abstractions/Services.Abstractions.csproj", "Services.Abstractions/"]
COPY ["Services/Services.csproj", "Services/"]
RUN dotnet restore "SocialNetworkCore/SocialNetworkCore.csproj"
COPY . .
WORKDIR "/src/SocialNetworkCore"
RUN dotnet build "SocialNetworkCore.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "SocialNetworkCore.csproj" -c Release -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "SocialNetworkCore.dll"]