﻿using DbAccess.ConnectionFactory;
using DbMigrations;
using Services.Abstractions.ApplicationServices;

namespace SocialNetworkCore;

public class LifetimeEventsHostedService: IHostedService
{
  private readonly ILogger _logger;
  private readonly DbConnectionFactoryOptions _connectionFactoryOptions;
  private readonly IAppStateService _appStateService;
  private readonly IDbMigrationService _migrationService;

  public LifetimeEventsHostedService(
    ILogger<LifetimeEventsHostedService> logger,
    IAppStateService appStateService,
    IDbMigrationService migrationService,
    DbConnectionFactoryOptions connectionFactoryOptions)
  {
    _appStateService = appStateService;
    _migrationService = migrationService;
    _connectionFactoryOptions = connectionFactoryOptions;
    _logger = logger;
  }

  public Task StartAsync(CancellationToken cancellationToken)
  {
    _logger.LogInformation($"ConnectionString: {_connectionFactoryOptions.PgConnectionString}");

    if (!MigrateDb())
      _appStateService.SetAppState(AppState.Failed, "DB migration failed");
    else
      _appStateService.SetAppState(AppState.Ready);

    return Task.CompletedTask;
  }

  public Task StopAsync(CancellationToken cancellationToken) => Task.CompletedTask;

  private bool MigrateDb()
  {
    try
    {
      _migrationService.Run();
      return true;
    }
    catch
    {
      return false;
    }
  }
}