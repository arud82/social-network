﻿using Microsoft.AspNetCore.Mvc;
using System.Text.Json;
using Services.Abstractions.ApplicationServices;

namespace SocialNetworkCore.Middleware;

public class CheckAppStateMiddleware
{
  private readonly IAppStateService _appStateService;
  private readonly RequestDelegate _next;

  public CheckAppStateMiddleware(RequestDelegate next, IAppStateService appStateService)
  {
    _next = next;
    _appStateService = appStateService;
  }

  public async Task InvokeAsync(HttpContext httpContext)
  {
    if (_appStateService.State == AppState.Ready)
    {
      await _next(httpContext);
      return;
    }

    var response = httpContext.Response;

    response.Clear();
    response.StatusCode = StatusCodes.Status503ServiceUnavailable;
    response.ContentType = "application/problem+json";

    var problemDetails = new ProblemDetails
    {
      Status = response.StatusCode,
      Type = "https://httpstatuses.com/503",
      Title = "App is unavailable",
      Detail = $"App is in {_appStateService.State} state. {_appStateService.ReasonFailed}",
      Instance = httpContext.Request.Path
    };

    var json = JsonSerializer.Serialize(problemDetails);
    await response.WriteAsync(json);
  }
}