﻿using Entities;
using ValueObjects;

namespace Repositories.Abstractions;

public interface IUserRepository
{
  Task Add(User user);
  Task<User?> Get(UserId userId);
  Task<IEnumerable<User>> Search(string? firstNamePrefix, string? secondNamePrefix);
  Task<int> Count(string? firstNamePrefix, string? secondNamePrefix);
}