﻿namespace ValueObjects;

public class UserId
{
  public Guid Value { get; }

  public UserId(Guid value)
  {
    Value = value;
  }

  public static UserId? Create(string value) => Guid.TryParse(value, out var result) ? new UserId(result) : null;

  public override string ToString() => Value.ToString();
}