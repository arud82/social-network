﻿using Services.Abstractions.ApplicationServices;

namespace Services.ApplicationServices;

internal class PasswordHasherService: IPasswordHasherService
{
  public string HashPassword(string password) => BCrypt.Net.BCrypt.HashPassword(password);

  public bool VerifyPassword(string password, string passwordHash) => BCrypt.Net.BCrypt.Verify(password, passwordHash);
}