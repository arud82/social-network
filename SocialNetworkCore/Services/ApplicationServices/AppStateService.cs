﻿using Services.Abstractions.ApplicationServices;
using Microsoft.Extensions.Logging;

namespace Services.ApplicationServices;

internal class AppStateService: IAppStateService
{
  private readonly ILogger _logger;

  public AppState State { get; private set; }
  public DateTime StartTime { get; private set; }
  public string ReasonFailed { get; private set; } = "";

  public AppStateService(ILogger<AppStateService> logger)
  {
    _logger = logger;
  }

  public void SetAppState(AppState state, string reasonFailed = "")
  {
    State = state;
    ReasonFailed = reasonFailed;
    StartTime = DateTime.Now;

    if (state == AppState.Failed && reasonFailed != string.Empty)
      _logger.LogError(reasonFailed);
  }
}