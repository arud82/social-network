﻿using Microsoft.Extensions.DependencyInjection;
using Services.Abstractions.ApplicationServices;
using Services.Abstractions.UserServices;
using Services.ApplicationServices;
using Services.UserServices;

namespace Services;

public static class DiServices
{
  public static void AddServices(this IServiceCollection services)
  {
    services.AddSingleton<IAppStateService, AppStateService>();

    services.AddScoped<IUserService, UserService>();
    services.AddScoped<IAuthenticationService, AuthenticationService>();

    services.AddScoped<IPasswordHasherService, PasswordHasherService>();
  }
}