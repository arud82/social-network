﻿using Entities;
using Exceptions;
using Repositories.Abstractions;
using Services.Abstractions.ApplicationServices;
using Services.Abstractions.UserServices;
using ValueObjects;

namespace Services.UserServices;

internal class UserService: IUserService
{
  private readonly IPasswordHasherService _passwordHasherService;
  private readonly IUserRepository _userRepository;

  public UserService(IPasswordHasherService passwordHasherService, IUserRepository userRepository)
  {
    _passwordHasherService = passwordHasherService;
    _userRepository = userRepository;
  }

  public async Task<User> Register(RegisterUserDto dto)
  {
    var passwordHash = _passwordHasherService.HashPassword(dto.Password);

    var user = new User(
      id: new UserId(Guid.NewGuid()),
      passwordHash: passwordHash,
      firstName: dto.FirstName,
      secondName: dto.SecondName,
      birthDate: dto.BirthDate,
      biography: dto.Biography,
      city: dto.City);

    await _userRepository.Add(user);

    return user;
  }

  public async Task<User> Get(UserId userId)
  {
    var user = await _userRepository.Get(userId);

    if (user == null)
      throw NotFoundException.Create<User>(userId.Value);

    return user;
  }

  public Task<IEnumerable<User>> Search(string? firstNamePrefix, string? secondNamePrefix) =>
    _userRepository.Search(firstNamePrefix, secondNamePrefix);

  public Task<int> Count(string? firstNamePrefix, string? secondNamePrefix) => _userRepository.Count(firstNamePrefix, secondNamePrefix);
}