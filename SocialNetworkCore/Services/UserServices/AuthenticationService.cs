﻿using Entities;
using Exceptions;
using Services.Abstractions.ApplicationServices;
using Services.Abstractions.UserServices;
using ValueObjects;

namespace Services.UserServices;

internal class AuthenticationService: IAuthenticationService
{
  private readonly IUserService _userService;
  private readonly IPasswordHasherService _passwordHasherService;

  public AuthenticationService(IUserService userService, IPasswordHasherService passwordHasherService)
  {
    _userService = userService;
    _passwordHasherService = passwordHasherService;
  }

  public async Task<User> Authenticate(UserId userId, string password)
  {
    var user = await _userService.Get(userId);

    if (!_passwordHasherService.VerifyPassword(password, user.PasswordHash))
      throw NotFoundException.Create<User>(userId.Value);

    return user;
  }
}