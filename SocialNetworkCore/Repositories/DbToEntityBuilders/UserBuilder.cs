﻿using DbAccess.Tables;
using Entities;
using ValueObjects;

namespace DbAccess.DbToEntityBuilders;

internal class UserBuilder
{
  public User Build(TbUser tb) =>
    new(
      id: new UserId(tb.id),
      passwordHash: tb.password_hash,
      firstName: tb.first_name,
      secondName: tb.second_name,
      birthDate: DateOnly.FromDateTime(tb.birth_date),
      biography: tb.biography,
      city: tb.city);
}