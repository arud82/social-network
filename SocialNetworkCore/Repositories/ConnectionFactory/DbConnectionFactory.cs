﻿using System.Data;
using Npgsql;

namespace DbAccess.ConnectionFactory;

public interface IDbConnectionFactory
{
  public Task<IDbConnection> CreateConnection();
}

internal class DbConnectionFactory: IDbConnectionFactory
{
  private readonly DbConnectionFactoryOptions _options;

  public DbConnectionFactory(DbConnectionFactoryOptions options)
  {
    _options = options;
  }

  public async Task<IDbConnection> CreateConnection()
  {
    var connection = new NpgsqlConnection(_options.PgConnectionString);
    await connection.OpenAsync();
    return connection;
  }
}