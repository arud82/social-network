﻿namespace DbAccess.Tables;

// ReSharper disable InconsistentNaming

internal class TbUser
{
  public Guid id { get; set; }
  public string password_hash { get; set; } = "";
  public string first_name { get; set; } = "";
  public string second_name { get; set; } = "";
  public DateTime birth_date { get; set; }
  public string biography { get; set; } = "";
  public string city { get; set; } = "";
}