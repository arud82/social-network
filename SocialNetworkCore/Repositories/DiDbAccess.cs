﻿using DbAccess.ConnectionFactory;
using DbAccess.DbToEntityBuilders;
using DbAccess.Repositories;
using Microsoft.Extensions.DependencyInjection;
using Repositories.Abstractions;

namespace DbAccess;

public static class DiDbAccess
{
  public static void AddDbAccess(this IServiceCollection services)
  {
    services.AddSingleton<IDbConnectionFactory, DbConnectionFactory>();

    services.AddScoped<IUserRepository, UserRepository>();

    services.AddScoped<UserBuilder>();
  }
}