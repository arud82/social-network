﻿using System.Data;
using System.Text;
using Dapper;
using DbAccess.ConnectionFactory;
using DbAccess.DbToEntityBuilders;
using DbAccess.Tables;
using Entities;
using Repositories.Abstractions;
using ValueObjects;

namespace DbAccess.Repositories;

internal class UserRepository: IUserRepository
{
  private readonly IDbConnectionFactory _connectionFactory;
  private readonly UserBuilder _userBuilder;

  public UserRepository(IDbConnectionFactory connectionFactory, UserBuilder userBuilder)
  {
    _connectionFactory = connectionFactory;
    _userBuilder = userBuilder;
  }

  public async Task Add(User user)
  {
    var query = "insert into \"user\"(id, first_name, second_name, birth_date, biography, city, password_hash) " +
                "values(@id, @first_name, @second_name, @birth_date, @biography, @city, @password_hash)";

    var parameters = new DynamicParameters();
    parameters.Add("id", user.Id.Value, DbType.Guid);
    parameters.Add("first_name", user.FirstName, DbType.String);
    parameters.Add("second_name", user.SecondName, DbType.String);
    parameters.Add("birth_date", user.BirthDate, DbType.Date);
    parameters.Add("biography", user.Biography, DbType.String);
    parameters.Add("city", user.City, DbType.String);
    parameters.Add("password_hash", user.PasswordHash, DbType.String);

    using var connection = await _connectionFactory.CreateConnection();
    await connection.ExecuteAsync(query, parameters);
  }

  public async Task<User?> Get(UserId userId)
  {
    var query = "select * from \"user\" where id = @id";
    
    var parameters = new DynamicParameters();
    parameters.Add("id", userId.Value, DbType.Guid);

    using var connection = await _connectionFactory.CreateConnection();
    var tbUser = (await connection.QueryAsync<TbUser>(query, parameters)).FirstOrDefault();

    if (tbUser == null)
      return null;

    return _userBuilder.Build(tbUser);
  }

  public async Task<IEnumerable<User>> Search(string? firstNamePrefix, string? secondNamePrefix)
  {
    if (string.IsNullOrWhiteSpace(firstNamePrefix) && string.IsNullOrWhiteSpace(secondNamePrefix))
      return new List<User>();

    var sb = new StringBuilder();
    sb.Append("select * from \"user\" where ");

    bool hasCondition = false;
    var parameters = new DynamicParameters();

    if (!string.IsNullOrWhiteSpace(firstNamePrefix))
    {
      sb.Append("first_name like @firstNamePrefix");
      parameters.Add("@firstNamePrefix", $"{firstNamePrefix}%", DbType.String);
      hasCondition = true;
    }

    if (!string.IsNullOrWhiteSpace(secondNamePrefix))
    {
      if (hasCondition)
        sb.Append(" and ");

      sb.Append("second_name like @secondNamePrefix");
      parameters.Add("@secondNamePrefix", $"{secondNamePrefix}%", DbType.String);
    }

    sb.Append(" order by id");

    var query = sb.ToString();

    using var connection = await _connectionFactory.CreateConnection();
    var tbUsers = await connection.QueryAsync<TbUser>(query, parameters);

    return tbUsers.Select(_userBuilder.Build);
  }

  public async Task<int> Count(string? firstNamePrefix, string? secondNamePrefix)
  {
    if (string.IsNullOrWhiteSpace(firstNamePrefix) && string.IsNullOrWhiteSpace(secondNamePrefix))
      return 0;

    var sb = new StringBuilder();
    sb.Append("select count(1) from \"user\" where ");

    bool hasCondition = false;
    var parameters = new DynamicParameters();

    if (!string.IsNullOrWhiteSpace(firstNamePrefix))
    {
      sb.Append("first_name like @firstNamePrefix");
      parameters.Add("@firstNamePrefix", $"{firstNamePrefix}%", DbType.String);
      hasCondition = true;
    }

    if (!string.IsNullOrWhiteSpace(secondNamePrefix))
    {
      if (hasCondition)
        sb.Append(" and ");

      sb.Append("second_name like @secondNamePrefix");
      parameters.Add("@secondNamePrefix", $"{secondNamePrefix}%", DbType.String);
    }

    var query = sb.ToString();

    using var connection = await _connectionFactory.CreateConnection();
    var res = await connection.QueryAsync<int>(query, parameters);

    return res.FirstOrDefault();
  }
}