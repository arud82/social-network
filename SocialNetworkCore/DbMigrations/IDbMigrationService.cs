﻿namespace DbMigrations;

public interface IDbMigrationService
{
  void Run();
}