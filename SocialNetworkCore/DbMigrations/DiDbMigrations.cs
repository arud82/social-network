﻿using Microsoft.Extensions.DependencyInjection;

namespace DbMigrations;

public static class DiDbMigrations
{
  public static void AddDbMigrationServices(this IServiceCollection services)
  {
    services.AddTransient<IDbMigrationService, DbMigrationService>();
  }
}