﻿DO $$
BEGIN

IF EXISTS (SELECT 1 FROM information_schema.tables WHERE table_name = 'user')
  THEN
    RAISE LOG 'table already exists.';
  ELSE

	CREATE TABLE "user" (
	  id uuid not null,
    first_name varchar not null,
    second_name varchar not null,
    birth_date date not null,
    biography varchar not null,
    city varchar not null,
    password_hash varchar not null,
    CONSTRAINT pk_user_id PRIMARY KEY (id)
		);

END IF;

END$$
Language plpgsql;