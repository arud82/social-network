﻿DO $$
BEGIN

alter table "user" alter biography drop not null;

END$$
Language plpgsql;