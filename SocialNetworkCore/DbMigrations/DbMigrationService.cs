﻿using FlexMigrations;
using Microsoft.Extensions.Logging;
using Npgsql;

namespace DbMigrations;

internal class DbMigrationService: IDbMigrationService
{
  private readonly ILoggerFactory _loggerFactory;
  private readonly DbConnectionFactory<NpgsqlConnection> _connectionFactory;

  public DbMigrationService(ILoggerFactory loggerFactory, MigrationOptions options)
  {
    _loggerFactory = loggerFactory;
    _connectionFactory = new DbConnectionFactory<NpgsqlConnection>(options.PgConnectionString);
  }

  public void Run()
  {
    RunMigrations();
  }

  private void RunMigrations()
  {
    const bool lockDb = false;

    MigrationManager migrationManager = new MigrationManager(_loggerFactory);

    if (!migrationManager.RunMigrations(_connectionFactory, ErrorPolicy.AllOrNothing, lockDb))
      throw new ApplicationException("DB Migration failed");
  }
}