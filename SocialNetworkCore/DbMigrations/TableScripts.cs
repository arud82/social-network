﻿using FlexMigrations;
using System.Text.RegularExpressions;

namespace DbMigrations;

internal class TableScripts: IVersionTableScripts
{
  private const string TableName = "settings";
  private const string KeyName = "param_key";
  private const string KeyValue = "DB_VERSION";
  private const string ValueName = "param_value";
  private const string DateName = "date_last_changed";

  public List<string>? CreateScripts => null;

  public string CheckExistence =>
    $"SELECT EXISTS(SELECT 1  FROM   information_schema.tables  WHERE  table_name = '{TableName}')";

  public string GetDbVersion => $"select {ValueName} from {TableName} where {KeyName} = '{KeyValue}'";

  public string SetDbVersion(int version, string scriptName) =>
    $"UPDATE {TableName} SET {ValueName} = '{scriptName}', {DateName} = CURRENT_TIMESTAMP WHERE {KeyName} = '{KeyValue}'";

  public bool ProcessCheckExistenceRes(object sqlRes) => (bool)sqlRes;

  public int ProcessGetDbVersionRes(object sqlRes) => int.Parse(Regex.Match((string)sqlRes, "(\\d+)_").Groups[1].Value);
}