﻿namespace Services.Abstractions.ApplicationServices;

public interface IAppStateService
{
  AppState State { get; }
  DateTime StartTime { get; }
  string ReasonFailed { get; }

  void SetAppState(AppState state, string reasonFailed = "");
}