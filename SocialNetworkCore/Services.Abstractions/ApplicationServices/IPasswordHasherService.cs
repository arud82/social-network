﻿namespace Services.Abstractions.ApplicationServices;

public interface IPasswordHasherService
{
  string HashPassword(string password);
  bool VerifyPassword(string password, string passwordHash);
}