﻿namespace Services.Abstractions.ApplicationServices;

public enum AppState
{
  Starting,
  Ready,
  Failed,
  Stopping
}