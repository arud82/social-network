﻿using Entities;
using ValueObjects;

namespace Services.Abstractions.UserServices;

public interface IUserService
{
  Task<User> Register(RegisterUserDto dto);
  Task<User> Get(UserId userId);
  Task<IEnumerable<User>> Search(string? firstNamePrefix, string? secondNamePrefix);
  Task<int> Count(string? firstNamePrefix, string? secondNamePrefix);
}