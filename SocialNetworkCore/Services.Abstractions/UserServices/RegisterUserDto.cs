﻿namespace Services.Abstractions.UserServices;

public record RegisterUserDto(
  string FirstName, 
  string SecondName, 
  DateOnly BirthDate, 
  string Biography, 
  string City, 
  string Password);