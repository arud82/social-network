﻿using Entities;
using ValueObjects;

namespace Services.Abstractions.UserServices;

public interface IAuthenticationService
{
  Task<User> Authenticate(UserId userId, string password);
}