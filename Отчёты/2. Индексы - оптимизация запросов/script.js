import { URL } from 'https://jslib.k6.io/url/1.0.0/index.js';
import http from 'k6/http';
import { sleep } from 'k6';
import { randomString } from 'https://jslib.k6.io/k6-utils/1.2.0/index.js';

function getName(length)
{
  const vowels = `аеиоуыэюя`;
  const consonants = `бвгджзйклмнпрстфхцчшщ`;
  
  let name = "";
  
  for (let i = 0; i < length; i++) {
    if (i%2 == 0)
      name += randomString(1, consonants);
    else
      name += randomString(1, vowels);
  }

  return name[0].toUpperCase() + name.substring(1);
}

export default function () {
  const url = new URL('http://localhost:9001/user/search');
  
  const firstNamePrefix = getName(1);
  const lastNamePrefix = getName(2);
  
  //console.log(`Prefixes are: ${firstNamePrefix} ${lastNamePrefix}`);

  url.searchParams.append('first_name', firstNamePrefix);
  url.searchParams.append('last_name', lastNamePrefix);

  const res = http.get(url.toString());
  
  //sleep(1);
}