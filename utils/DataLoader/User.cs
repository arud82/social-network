﻿namespace DataLoader;

public class User
{
  public Guid Id { get; }
  public string PasswordHash { get; } 
  public string FirstName { get; }
  public string SecondName { get; }
  public DateOnly BirthDate { get; }
  public string City { get; }

  public User(Guid id, string passwordHash, string firstName, string secondName, DateOnly birthDate, string city)
  {
    Id = id;
    PasswordHash = passwordHash;
    FirstName = firstName;
    SecondName = secondName;
    BirthDate = birthDate;
    City = city;
  }

  public override string ToString()
  {
    return
      $"{nameof(Id)}: {Id}, {nameof(PasswordHash)}: {PasswordHash}, {nameof(FirstName)}: {FirstName}, {nameof(SecondName)}: {SecondName}, {nameof(BirthDate)}: {BirthDate}, {nameof(City)}: {City}";
  }
}