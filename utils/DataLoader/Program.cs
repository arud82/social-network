﻿using Dapper;
using Npgsql;
using System.Data;
using System.Diagnostics;
using System.Text;

namespace DataLoader;

// ReSharper disable once ClassNeverInstantiated.Global
internal class Program
{
  private static readonly Random Rnd = new();

  public static void Main()
  {
    var sw = new Stopwatch();
    sw.Start();

    try
    {
      var users = ReadUsers();
      //InsertData(users);
      WriteUsers(users);
    }
    catch (Exception ex)
    {
      Console.WriteLine(ex.ToString());
    }

    sw.Stop();
    Console.WriteLine(sw.Elapsed);
  }

  private static void WriteUsers(IEnumerable<User> users)
  {
    var file = File.OpenWrite("p2.txt");

    int i = 0;

    foreach (var user in users)
    {
      var str = $"{user.Id},{user.FirstName},{user.SecondName},{user.BirthDate.ToString("yyyyMMdd")},null,{user.City},{user.PasswordHash}\n";
      byte[] bytes = Encoding.UTF8.GetBytes(str);
      file.Write(bytes);

      i++;

      if (i % 100_000 == 0)
        Console.WriteLine($"{i} records written");
    }

    file.Close();
  }

  private static readonly string PasswordHash = HashPassword("password");

  private static User CreateUser(string fullName, int age, string city)
  {
    var nameParts = fullName.Split(' ');

    string firstName = nameParts[1];
    string lastName = nameParts[0];

    return new User(
      Guid.NewGuid(),
      PasswordHash,
      firstName,
      lastName,
      GetBirthDate(age),
      city);
  }

  private static void InsertData(IEnumerable<User> users)
  {
    const string connectionString = "Host=localhost;Port=5432;Username=postgres;Password=postgres;Database=social-network-core;";

    using var connection = new NpgsqlConnection(connectionString);
    connection.Open();

    var query = "insert into \"user\"(id, first_name, second_name, birth_date, city, password_hash) " +
                "values(@id, @first_name, @second_name, @birth_date, @city, @password_hash)";

    int i = 0;

    foreach (var user in users)
    {
      var parameters = new DynamicParameters();
      parameters.Add("id", user.Id, DbType.Guid);
      parameters.Add("first_name", user.FirstName, DbType.String);
      parameters.Add("second_name", user.SecondName, DbType.String);
      parameters.Add("birth_date", user.BirthDate, DbType.Date);
      parameters.Add("city", user.City, DbType.String);
      parameters.Add("password_hash", user.PasswordHash, DbType.String);  

      connection.Execute(query, parameters);

      i++;

      if (i % 10000 == 0)
        Console.WriteLine($"{i} records inserted");
    }
  }

  private static string HashPassword(string password) => BCrypt.Net.BCrypt.HashPassword(password);

  private static DateOnly GetBirthDate(int age)
  {
    var now = DateTime.Now;
    var endYear = now.Year - age;
    var endDate = new DateOnly(endYear, now.Month, now.Day - 1);
    return endDate.AddDays(-Rnd.Next(0, 364));
  }
  
  private static IEnumerable<User> ReadUsers()
  {
    var lines = File.ReadLines("people.txt");

    var list = new List<User>();

    foreach (var line in lines)
    {
      var parts = line.Split(',');
      var name = parts[0];
      var age = int.Parse(parts[1]);
      var city = parts[2];

      yield return CreateUser(name, age, city);
    }
  }
}