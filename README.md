# SocialNetwork

## Запуск
docker compose up
или
start.bat

Стартует и pg, и бэк.
БД приложения автоматически создаётся бэком при его запуске.

## Swagger
localhost:9001/swagger

## Postman
https://api.postman.com/collections/25503409-3fb97c98-f2a3-4cb3-8c38-1e31cb4e243e?access_key=PMAT-01HEK32YKZ8BRV7YZ0FQAMWXPT

## Остановка
docker compose down
или
stop.bat

